REM ##################################################################
REM FILENAME   : make_minimal_LaTeX_example.bat
REM AUTHOR     : Brian Denton <dentonb@mskcc.org>
REM DATE       : 05/02/2011
REM DESCRIPTION: Batch file containing commands to create
REM              minimal_LaTeX_example.pdf and delete intermediate
REM              files.
REM ##################################################################

pdflatex minimal_LaTeX_example.tex

REM Delete intermediate files
del minimal_LaTeX_example.log
del minimal_LaTeX_example.ent
del minimal_LaTeX_example.aux

REM END OF FILE
