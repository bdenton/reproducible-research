%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FILENAME   : LaTeX_example.tex
% AUTHOR     : Brian Denton <dentonb@mskcc.org>
% DATE       : 02/21/2011
% DESCRIPTION: Second example using LaTeX.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass{article}
\usepackage{verbatim}
\usepackage{hyphenat}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{endnotes}
\usepackage{listings}
\usepackage{multirow}

\begin{document}

\setlength{\parindent}{0pt} %Set indent width to 0 points
\parskip = \baselineskip    %Skip line between paragraphs
\hyphenpenalty = 10000      %Suppress hyphenations by setting hyphenation penalty to large number

\title{A \LaTeX\ Demo}
\author{Brian Denton\\ \\Memorial Sloan-Kettering Cancer Center}
\maketitle


\section{Introduction}

\LaTeX\ is an open-source typesetting program. The syntax is similar to many other markup languages. It is used commonly in academia for writing journal articles. Indeed, many journals provide their own \LaTeX\ style template.  \LaTeX\ is also used for creating presentations with the \verb\beamer\ package.

\LaTeX\ is well-suited for scientific and technical writing because it can produce very nicely-formatted mathematical equations.  In particular, the \verb\amsmath\ package uses typesetting conventions recommended by the American Mathematical Society for publication-quality equations.

\newpage

\section{ Parts of a \LaTeX\ document}

You can embed images in your \LaTeX\ document.

\includegraphics[scale=0.2]{LaTeX_schematic.png}

\newpage

\section{Links}

You can define links to other sections within your document or hyperlinks to websites.

Information on writing equations is in section ~\ref{sec:equations}.

\url{http://www.youtube.com/watch?v=ax0tDcFkPic&feature=related}

\href{http://www.youtube.com/watch?v=PbODigCZqL8&feature=related}{Biostatistics vs. Lab Research}

You can also link to a footnote.\footnote{This is the first footnote.} And here is the second foonote.\footnote{This is the second footnote.}

Or if you prefer to use endnotes you can use the \verb\endnotes\ package to get endnotes.\endnote{This is the first endnote.} And, of course, multiple endnotes are allowed.\endnote{This is the second endnote.} Then you include the statemtent: 
\begin{lstlisting}
\theendnotes
\end{lstlisting}
in the document where you would like the endnotes to appear.

\newpage

\section{Mathematics}

\subsection{ amsmath package }

The \AmS-\LaTeX\ package \verb\amsmath\ provides an extended set of mathematical characters and typesetting features.

You can use letters from the Greek alphabet: $\alpha, \beta, \gamma, \Lambda, \Phi, \Theta, \dots$

You can do superscripts, subscripts, and accents:  $\hat{\beta_{i}}, \bar{x}, \epsilon_{ij} \sim \mathnormal{N}\negmedspace\left(0,\sigma^2\right)$

You can create matrices:

\[
\mathbf{A} = \begin{bmatrix}
a_{11} & a_{12} & a_{13} \\
a_{21} & a_{22} & a_{23} \\
a_{31} & a_{32} & a_{33}
\end{bmatrix}
\]

You can use set notation:
\[ \mathbf{A} = \left\{1,2,3\right\} \]
\[ \mathbf{B} = \left\{2,4,6\right\} \]
\[ \mathbf{A} \cup \mathbf{B} = \left\{1,2,3,4,6\right\} \]
\[ \mathbf{A} \cap \mathbf{B} = \left\{2\right\} \]

\newpage

\subsection{ Equations }
\label{sec:equations}

You can create an inline equation $\cos^2\left(\theta\right) + \sin^2\left(\theta\right) = 1$.

You can create an unnumbered display equation:

\[
\left(\hat{\alpha},\hat{\beta}\right) = \arg\min\left\{ \sum_{i=1}^{N} \left( y_{i} - \alpha - \sum_{j} \beta_{j} x_{ij} \right) ^ 2  \right\}
\qquad \text{subject to} \sum_{j} \left|\beta_{j}\right| \leq t
\]


You can also create a numbered display equation:
\begin{equation} \label{E:Eq1}
\tan x = \frac{\sin x}{\cos x}
\end{equation}

The advantage of numbering equations is that you can use references to the equations: See~\eqref{E:Eq1}.

You can align equations at any point by indicating the alignmenmt point with an ampersand:

% \notag supresses equation numbering

\begin{align}
	y &= ( a + x )( b - x )  \notag \\
	  &= ab - ax + bx - x^2  \notag
\end{align}

\begin{align}
	f(x) &\leq ( a + x)(b - x)    \notag \\  
	     &\leq ab - ax + bx - x^2 \notag
\end{align}

\newpage

\section{Tables}

\begin{tabular}{ | l | c || r| }
  \hline
  Left-aligned & Centered & Right-aligned \\
  \hline
  $\alpha_{21}$ & $\alpha_{22}$ & $\alpha_{23}$ \\
  \hline
  $\sigma_{31}^{2}$ & $\sigma_{32}^{2}$ & $\sigma_{33}^{2}$ \\
  \hline
\end{tabular}

\begin{tabular}{|l|r|}
\hline
\multicolumn{2}{|c|}{Menu (this row spans multiple columns)} \\
\hline
Ham sandwich & \$6.50 \\
Tomato soup & \$3.00 \\
Ceasar salad & \$6.00\\
\hline
Ice cream & \$2.00 \\
Cookies & \$0.50 \\
\hline
Hot tea & \$0.75 \\
Coffee & \$0.75\\
\hline
\end{tabular}


\begin{tabular}{|l|l|r|}
\hline
\multicolumn{3}{|c|}{Menu (this row spans multiple columns)} \\
\hline
\multirow{3}{*}{Lunch (column spans multiple rows)} & Ham sandwich & \$6.50 \\
                       & Tomato soup & \$3.00 \\
                       & Ceasar salad & \$6.00\\
\hline

\multirow{2}{*}{Dessert/Snacks} & Ice cream & \$2.00 \\
                                & Cookies & \$0.50 \\
\hline

\multirow{2}{*}{Beverages} & Hot tea & \$0.75 \\
                           & Coffee & \$0.75\\
\hline

\end{tabular}



\theendnotes


%\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
%    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
	
%\section{\BibTeX\ Bibliography}
%\label{bibliography}


%BiBTeX

\end{document}
