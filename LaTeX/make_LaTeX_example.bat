REM ##################################################################
REM FILENAME   : make_LaTeX_example.bat
REM AUTHOR     : Brian Denton <dentonb@mskcc.org>
REM DATE       : 05/02/2011
REM DESCRIPTION: Batch file containing commands to create
REM              LaTeX_example.pdf and delete intermediate
REM              files.
REM ##################################################################

pdflatex LaTeX_example.tex
pdflatex LaTeX_example.tex

REM Delete intermediate files
del LaTeX_example.log
del LaTeX_example.ent
del LaTeX_example.aux
del LaTeX_example.out

REM END OF FILE
