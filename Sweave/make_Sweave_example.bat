REM pdflatex needs to know the location of Sweave.sty
REM create a variable for the path to Sweave.sty
set SWEAVE="C:/Program Files/R/R-2.13.0rc/share/texmf/tex/latex/"

REM The Sweave() function "weaves" the Sweave code using LaTeX to generate
REM a formatted pdf. 

R CMD Sweave Sweave_example.Rnw
pdflatex -include-directory=%SWEAVE% Sweave_example.tex

REM Remove intermediate files
del Sweave_example.tex
del Sweave_example.log
del Sweave_example.aux
del Sweave_example-???.*
del Rplots.pdf

REM The Stangle() function "tangles" the Sweave code extracting all the R code
REM to a new file.

R CMD Stangle Sweave_example.Rnw

REM END OF FILE
