REM ##################################################################
REM FILENAME   : make_minimal_Sweave_example.bat
REM AUTHOR     : Brian Denton <dentonb@mskcc.org>
REM DATE       : 05/02/2011
REM DESCRIPTION: Batch file containing commands to create
REM              minimal_Sweave_example.pdf and delete intermediate
REM              files.
REM ##################################################################

REM pdflatex needs to know the location of Sweave.sty
REM create a variable for the path to Sweave.sty
set SWEAVE="C:/Program Files/R/R-2.13.0rc/share/texmf/tex/latex/"

R CMD Sweave minimal_Sweave_example.Rnw
pdflatex -include-directory=%SWEAVE% minimal_Sweave_example.tex

REM Remove intermediate files
REM

REM END OF FILE
